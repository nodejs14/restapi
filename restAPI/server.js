const express = require('express')
const app = express()

const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

var userOjb = [ {"id":1, "name":"vipawadee", "email":"vip@gmail.com"},
             {"id":2, "name":"nattawat", "email":"tom@gmail.com"}
           ]

// GET DATA
app.get('/', function (req, res){
    res.json(userOjb)
})

// GET BYId
app.get('/:id', function (req, res){
    let id = parseInt(req.params.id);
    console.log('id:'+req.params.id)
    const index = userOjb.find(i=> i.id==id)
    if(!index){
        res.status(404).send('id not found')
    }else{
         res.send(index)
    }
})

// POST DATAAPP
app.post('/', function(req,res){
    let name =req.body.name
    let email=req.body.email
    console.log(name+' '+email)
    let postdata ={
        "id":userOjb.length+1,
        "name":name,
        "email":email
    }
    userOjb.push(postdata)
    res.json(userOjb)
})

//PUT/UPDATE DATA
app.put('/:id', function(req,res){
    let id = parseInt(req.params.id);
    console.log('id:'+req.params.id)
    const index = userOjb.find(i=> i.id==id)

    if(!index){
        res.status(404).send('id not found')
    }else{
         index.name = req.body.name
         index.email = req.body.email
         res.send(index)
    }
})

// DELETE DATA
app.delete('/:id', function(req,res){
    let id = parseInt(req.params.id);
    console.log('id:'+req.params.id)

    const index = userOjb.find(i=> i.id==id)
    if(!index){
        res.status(404).send('id not found')
    }else{
        const indexof = userOjb.indexOf(index)
        userOjb.splice(indexof,1)
        res.status(204).send('delete success..')
    }
})

//server listenning
app.listen(9000, function () {
    console.log('Example app listening on port 9000!')
  })